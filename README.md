# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* 
In Project 1 we will build a resource sharing network that consists of servers, which can communicate with each other, and clients which can communicate with the servers. The system will be called EZShare.
Project 2 is developed on top of Project 1. A secure socket is implemented along side the existing port, responsible for making secure connections with clients and other servers. Also implemented new functions SUBSCRIBE/UNSUBSCRIBE. For details refer to the specification. 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
* How to run test commands:  
**START  a server:**  
java -cp ezshare.jar EZShare.Server -port 3000 -sport 3781 -debug -connectionintervallimit 1 -exchangeinterval 600  
**PUBLISH a resource:**  
java -cp ezshare.jar EZShare.Client -publish -name "Unimelb website" -description "The main page for the
University of Melbourne" -uri http://www.unimelb.edu.au -tags web,html -debug


### Who do I talk to? ###

* Repo owner or admin
kinchan1210@gmail.com