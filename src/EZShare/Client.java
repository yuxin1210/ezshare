package EZShare;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.util.Arrays;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * This class implements the clients in EZShare, a resource sharing network. A
 * client can send requests to servers and receive responses from server. Such
 * requests include publishing, removing, sharing and querying resources. A
 * client can also notify a server the existence of other servers.
 *
 * @author yuxin
 *
 */
public class Client {
	// a folder where the fetched files are stored
	// private static final String downloadFolder = "download/";

	private static final String downloadFolder = "";
	// default host and port
	private static String ip = "localhost";
	private static int port = 3000;
	private static boolean debugON = false;
	// create Options object
	static Options options = new Options();
	private static final Logger clientLog = Logger.getGlobal();
	private static int subscriptionID = 0;

	public static void main(String[] args) {
		// System.setProperty("javax.net.ssl.keyStore",
		// "classes/clientKeystore.jks");
		// System.setProperty("javax.net.ssl.keyStorePassword", "comp90015");
		// // Location of the Java keystore file containing the collection of
		// // certificates trusted by this application (trust store).
		// System.setProperty("javax.net.ssl.trustStore",
		// "classes/serverKeystore.jks");
		// System.setProperty("javax.net.ssl.trustStorePassword", "comp90015");
		// System.setProperty("javax.net.debug", "all");

		InputStream keystoreInput = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("clientKeystore.jks");
		InputStream truststoreInput = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("serverKeystore.jks");
		try {
			setSSLFactories(keystoreInput, "comp90015", truststoreInput);
		} catch (Exception e3) {
			e3.printStackTrace();
		}
		try {
			keystoreInput.close();
			truststoreInput.close();
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		// add command line options
		options.addOption("channel", true, "channel");
		options.addOption("debug", false, "print debug information");
		options.addOption("description", true, "resource description");
		options.addOption("exchange", false, "exchange server list with server");
		options.addOption("fetch", false, "fetch resources from server");
		options.addOption("host", true, "server host, a domain name or IP address");
		options.addOption("name", true, "resource name");
		options.addOption("owner", true, "owner");
		options.addOption("port", true, "server port, an integer");
		options.addOption("publish", false, "publish resource on server");
		options.addOption("query", false, "query for resource on server");
		options.addOption("subscribe", false, "subscribe resources on server");
		options.addOption("remove", false, "remove resource from server");
		options.addOption("secret", true, "secret");
		options.addOption(Option.builder("servers").desc("server list,host1:port1,host2:port2,...").hasArgs()
				.valueSeparator(',').build());
		options.addOption("share", false, "share resource on server");
		options.addOption(
				Option.builder("tags").desc("resource tags, tag1,tag2,tag3,...").hasArgs().valueSeparator(',').build());
		options.addOption("uri", true, "resource URI");
		options.addOption("secure", false, "make a secure connection");

		// a command parser
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;
		Handler consoleHandler = new ConsoleHandler();
		consoleHandler.setLevel(Level.FINE);
		clientLog.addHandler(consoleHandler);
		// Prevent logs from processed by default Console handler.
		clientLog.setUseParentHandlers(false);
		clientLog.setLevel(Level.FINE);

		try {
			cmd = parser.parse(options, args);
			if (cmd.hasOption("port")) {
				port = Integer.parseInt(cmd.getOptionValue("port"));
			}
			if (cmd.hasOption("host")) {
				ip = cmd.getOptionValue("host");
			}
			if (cmd.hasOption("debug")) {
				debugON = true;
				clientLog.info("setting debug on");
			}
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		// SSLSocket sslsocket = null;
		Socket socket = null;
		DataOutputStream output = null;
		DataInputStream input = null;

		try {
			if (cmd.hasOption("secure")) {
				SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
				socket = (SSLSocket) sslsocketfactory.createSocket(ip, port);
			} else {
				socket = new Socket(ip, port);
			}
			output = new DataOutputStream(socket.getOutputStream());
			input = new DataInputStream(socket.getInputStream());

			JSONObject request = new JSONObject();
			// PUBLISH command
			if (cmd.hasOption("publish")) {
				publishRequest(request, cmd);
				// REMOVE command
			} else if (cmd.hasOption("remove")) {
				removeRequest(request, cmd);
				// QUERY command
			} else if (cmd.hasOption("query")) {
				queryRequest(request, cmd);
				// EXCHANGE command
			} else if (cmd.hasOption("exchange")) {
				exchangeRequest(request, cmd);
				// SHARE command
			} else if (cmd.hasOption("share")) {
				shareRequest(request, cmd);
				// FETCH command
			} else if (cmd.hasOption("fetch")) {
				fetchRequest(request, cmd);
			} else if (cmd.hasOption("subscribe")) {
				subscribeRequest(request, cmd);
				Thread t = new Thread(new ReadConsoleInput(output, debugON, clientLog));
				t.start();
			}
			if (debugON) {
				clientLog.fine("SENT: " + request.toJSONString());
			}

			// write JSON to server
			try {
				output.writeUTF(request.toJSONString());
				output.flush();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			// receive messages from server
			JSONParser jsonParser = new JSONParser();
			String message = null;
			if (socket != null) {
				while (true) {
					try {
						message = input.readUTF();
						if (debugON) {
							clientLog.fine("RECEIVED: " + message);
						}
						JSONObject msgObj = (JSONObject) jsonParser.parse(message);
						if (msgObj.containsKey("resourceSize")) {
							URI fileUri = new URI(msgObj.get("uri").toString());
							String severFilePath = fileUri.getPath();
							String fileName = severFilePath.substring(severFilePath.lastIndexOf('/') + 1);
							// The fetched file location
							String downloadPath = downloadFolder + fileName;
							// Create a RandomAccessFile to read and write the
							// output file.
							RandomAccessFile downloadingFile = new RandomAccessFile(downloadPath, "rw");
							// Find out how much size is remaining to get from
							// the server.
							long fileSizeRemaining = (Long) msgObj.get("resourceSize");
							int chunkSize = setChunkSize(fileSizeRemaining);
							// Represents the receiving buffer
							byte[] receiveBuffer = new byte[chunkSize];
							// Variable used to read if there are remaining size
							// left to read.
							int num;
							while ((num = input.read(receiveBuffer)) > 0) {
								// Write the received bytes into the
								// RandomAccessFile
								downloadingFile.write(Arrays.copyOf(receiveBuffer, num));
								// Reduce the file size left to read..
								fileSizeRemaining -= num;
								// Set the chunkSize again
								chunkSize = setChunkSize(fileSizeRemaining);
								receiveBuffer = new byte[chunkSize];
								// If you're done then break
								if (fileSizeRemaining == 0) {
									break;
								}
							}
							clientLog.fine("File saved at " + downloadPath);
							downloadingFile.close();
						}
					} catch (IOException e) {
						// e.printStackTrace();
						break;
					} catch (org.json.simple.parser.ParseException e) {
						e.printStackTrace();
					} catch (URISyntaxException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			clientLog.info("The client program to be shut down.");
		}
	}

	public static void publishRequest(JSONObject request, CommandLine cmd) {
		JSONObject resource = new JSONObject();
		request.put("command", "PUBLISH");
		if (cmd.hasOption("uri")) {
			resource.put("uri", cmd.getOptionValue("uri"));
		}
		if (cmd.hasOption("owner")) {
			resource.put("owner", cmd.getOptionValue("owner"));
		}
		if (cmd.hasOption("channel")) {
			resource.put("channel", cmd.getOptionValue("channel"));
		}
		if (cmd.hasOption("name")) {
			resource.put("name", cmd.getOptionValue("name"));
		}
		if (cmd.hasOption("description")) {
			resource.put("description", cmd.getOptionValue("description"));
		}
		if (cmd.hasOption("tags")) {
			String[] tags = cmd.getOptionValues("tags");
			JSONArray tagArray = new JSONArray();
			for (String tag : tags) {
				tagArray.add(tag);
			}
			resource.put("tags", tagArray);
		}
		request.put("resource", resource);
		clientLog.fine("publishing to " + ip + ":" + port);
	}

	public static void removeRequest(JSONObject request, CommandLine cmd) {
		JSONObject resource = new JSONObject();
		request.put("command", "REMOVE");
		resource.put("uri", cmd.getOptionValue("uri"));
		if (cmd.hasOption("owner")) {
			resource.put("owner", cmd.getOptionValue("owner"));
		}
		if (cmd.hasOption("channel")) {
			resource.put("channel", cmd.getOptionValue("channel"));
		}
		request.put("resource", resource);
		clientLog.fine("removing from " + ip + ":" + port);
	}

	public static void queryRequest(JSONObject request, CommandLine cmd) {
		JSONObject resource = new JSONObject();
		request.put("command", "QUERY");
		if (cmd.hasOption("uri")) {
			resource.put("uri", cmd.getOptionValue("uri"));
		}
		if (cmd.hasOption("owner")) {
			resource.put("owner", cmd.getOptionValue("owner"));
		}
		if (cmd.hasOption("channel")) {
			resource.put("channel", cmd.getOptionValue("channel"));
		}
		if (cmd.hasOption("name")) {
			resource.put("name", cmd.getOptionValue("name"));
		}
		if (cmd.hasOption("description")) {
			resource.put("description", cmd.getOptionValue("description"));
		}
		if (cmd.hasOption("tags")) {
			String[] tags = cmd.getOptionValues("tags");
			JSONArray tagArray = new JSONArray();
			for (String tag : tags) {
				tagArray.add(tag);
			}
			resource.put("tags", tagArray);
		}
		request.put("resourceTemplate", resource);
		request.put("relay", true);
		clientLog.fine("querying " + ip + ":" + port);
	}

	private static void subscribeRequest(JSONObject request, CommandLine cmd) {
		JSONObject resource = new JSONObject();
		request.put("command", "SUBSCRIBE");
		if (cmd.hasOption("uri")) {
			resource.put("uri", cmd.getOptionValue("uri"));
		}
		if (cmd.hasOption("owner")) {
			resource.put("owner", cmd.getOptionValue("owner"));
		}
		if (cmd.hasOption("channel")) {
			resource.put("channel", cmd.getOptionValue("channel"));
		}
		if (cmd.hasOption("name")) {
			resource.put("name", cmd.getOptionValue("name"));
		}
		if (cmd.hasOption("description")) {
			resource.put("description", cmd.getOptionValue("description"));
		}
		if (cmd.hasOption("tags")) {
			String[] tags = cmd.getOptionValues("tags");
			JSONArray tagArray = new JSONArray();
			for (String tag : tags) {
				tagArray.add(tag);
			}
			resource.put("tags", tagArray);
		}
		request.put("resourceTemplate", resource);
		request.put("relay", true);
		subscriptionID++;
		request.put("id", String.valueOf(subscriptionID));
		clientLog.fine("subscribing to " + ip + ":" + port);

	}

	public static void exchangeRequest(JSONObject request, CommandLine cmd) {
		request.put("command", "EXCHANGE");
		if (cmd.hasOption("servers")) {
			String[] servers = cmd.getOptionValues("servers");
			JSONArray serverArray = new JSONArray();
			for (String server : servers) {
				JSONObject aServer = new JSONObject();
				String serverHostname = server.split(":")[0];
				int serverPort = Integer.parseInt(server.split(":")[1]);
				aServer.put("hostname", serverHostname);
				aServer.put("port", serverPort);
				serverArray.add(aServer);
			}
			request.put("serverList", serverArray);
		}
		clientLog.fine("exchanging with " + ip + ":" + port);
	}

	public static void shareRequest(JSONObject request, CommandLine cmd) {
		JSONObject resource = new JSONObject();
		request.put("command", "SHARE");
		if (cmd.hasOption("secret")) {
			request.put("secret", cmd.getOptionValue("secret"));
		}
		if (cmd.hasOption("uri")) {
			resource.put("uri", cmd.getOptionValue("uri"));
		}
		if (cmd.hasOption("owner")) {
			resource.put("owner", cmd.getOptionValue("owner"));
		}
		if (cmd.hasOption("channel")) {
			resource.put("channel", cmd.getOptionValue("channel"));
		}
		if (cmd.hasOption("name")) {
			resource.put("name", cmd.getOptionValue("name"));
		}
		if (cmd.hasOption("description")) {
			resource.put("description", cmd.getOptionValue("description"));
		}
		if (cmd.hasOption("tags")) {
			String[] tags = cmd.getOptionValues("tags");
			JSONArray tagArray = new JSONArray();
			for (String tag : tags) {
				tagArray.add(tag);
			}
			resource.put("tags", tagArray);
		}
		request.put("resource", resource);
		clientLog.fine("sharing to " + ip + ":" + port);
	}

	public static void fetchRequest(JSONObject request, CommandLine cmd) {
		JSONObject resource = new JSONObject();
		request.put("command", "FETCH");
		resource.put("uri", cmd.getOptionValue("uri"));
		if (cmd.hasOption("channel")) {
			resource.put("channel", cmd.getOptionValue("channel"));
		}
		request.put("resourceTemplate", resource);
		clientLog.fine("fetching from " + ip + ":" + port);
	}

	/**
	 * This method determines the chunk size(in bytes) when fetching a file from
	 * the server. The chunk size will adjust to the remaining file size if the
	 * remaining size is smaller than the default chunk size. The default chunk
	 * size is 1024*1024 bytes(1 MiB).
	 *
	 * @param fileSizeRemaining
	 *            The size of the partial file to be downloaded
	 * @return Returns an integer representing the chunk size.
	 */
	public static int setChunkSize(long fileSizeRemaining) {
		// Determine the chunkSize
		int chunkSize = 1024 * 1024;

		// If the file size remaining is less than the chunk size
		// then set the chunk size to be equal to the file size.
		if (fileSizeRemaining < chunkSize) {
			chunkSize = (int) fileSizeRemaining;
		}

		return chunkSize;
	}

	public static void unsubscribeRequest(JSONObject request) {
		request.put("command", "UNSUBSCRIBE");
		request.put("id", String.valueOf(subscriptionID));
		clientLog.fine("unsubscribing to " + ip + ":" + port);
	}

	private static void setSSLFactories(InputStream keyStream, String keyStorePassword, InputStream trustStream)
			throws Exception {
		// Get keyStore
		KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

		// if your store is password protected then declare it (it can be null
		// however)
		char[] keyPassword = keyStorePassword.toCharArray();

		// load the stream to your store
		keyStore.load(keyStream, keyPassword);

		// initialize a trust manager factory with the trusted store
		KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		keyFactory.init(keyStore, keyPassword);

		// get the trust managers from the factory
		KeyManager[] keyManagers = keyFactory.getKeyManagers();

		// Now get trustStore
		KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());

		// if your store is password protected then declare it (it can be null
		// however)
		// char[] trustPassword = password.toCharArray();

		// load the stream to your store
		trustStore.load(trustStream, keyPassword);

		// initialize a trust manager factory with the trusted store
		TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustFactory.init(trustStore);

		// get the trust managers from the factory
		TrustManager[] trustManagers = trustFactory.getTrustManagers();

		// initialize an ssl context to use these managers and set as default
		SSLContext sslContext = SSLContext.getInstance("SSL");
		sslContext.init(keyManagers, trustManagers, null);
		SSLContext.setDefault(sslContext);
	}
}