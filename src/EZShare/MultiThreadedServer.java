package EZShare;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MultiThreadedServer implements Runnable {
	private static int counter = 0;
	private static long startTime = 0;
	private static String lastClientIp = null;
	private static ResourceManager resourceManager;
	private static int connectionIntervalLimit;
	private static String secret;
	private static boolean debugON;
	private List<String> serverRecords;
	// private static List<Subscriber> subscribers;
	// private int numberOfSubscriptions = 0;
	private static ConcurrentHashMap<Socket, Integer> clientSockets = new ConcurrentHashMap<Socket, Integer>();
	private static ConcurrentHashMap<Socket, Integer> clientSubscriptionHits = new ConcurrentHashMap<Socket, Integer>();
	private static ConcurrentHashMap<Socket, ArrayList<Subscriber>> clientSubscriptions = new ConcurrentHashMap<Socket, ArrayList<Subscriber>>();
	private static ConcurrentHashMap<String, Socket> outgoingConnections = new ConcurrentHashMap<String, Socket>();
	private ServerSocket serverSocket = null;
	private String ip;
	private int port;
	private int theOtherPort;
	private Logger serverLog;
	private boolean secure;

	public MultiThreadedServer(ServerSocket serverSocket, String ip, int theOtherPort, ResourceManager resourceManager,
			int connectionIntervalLimit, String secret, boolean debugON, List<String> serverRecords, Logger serverLog,
			boolean secure) {
		this.serverSocket = serverSocket;
		this.port = serverSocket.getLocalPort();
		this.ip = ip;
		this.theOtherPort = theOtherPort;
		MultiThreadedServer.resourceManager = resourceManager;
		MultiThreadedServer.connectionIntervalLimit = connectionIntervalLimit;
		MultiThreadedServer.secret = secret;
		MultiThreadedServer.debugON = debugON;
		this.serverRecords = serverRecords;
		this.serverLog = serverLog;
		this.secure = secure;
	}

	@Override
	public void run() {
		// wait for connection
		System.out.println("Waiting for connection");
		ServerExchangeTask serverExchangeTask = new ServerExchangeTask(ip + ":" + String.valueOf(port),
				Server.exchangeInterval, serverRecords, serverLog, secure, outgoingConnections);
		Thread serverExchangeThread = new Thread(serverExchangeTask);
		serverExchangeThread.start();
		try {
			while (true) {
				Socket client = serverSocket.accept();
				if (incrementClientCounter(client)) {
					Thread t = new Thread(() -> serveClient(client));
					t.start();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static synchronized boolean incrementClientCounter(Socket client) {
		boolean newClient = false;
		String currentClientIp = client.getInetAddress().getHostAddress();
		if (counter == 0) {
			startTime = System.nanoTime();
			lastClientIp = client.getInetAddress().getHostAddress();
			counter++;
			System.out.println("connected with Client " + counter);
			clientSockets.put(client, counter);
			newClient = true;
		} else {
			long estimatedTime = System.nanoTime() - startTime;
			// make sure each IP address cannot connect more than once
			// within the connection interval limit
			if (currentClientIp.equals(lastClientIp)
					&& TimeUnit.NANOSECONDS.toSeconds(estimatedTime) < connectionIntervalLimit) {
				try {
					client.close();
					newClient = false;
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				startTime = System.nanoTime();
				lastClientIp = currentClientIp;
				counter++;
				System.out.println("connected with Client " + counter);
				clientSockets.put(client, counter);
				newClient = true;
			}
		}
		return newClient;
	}

	/**
	 * This method implements the communication between a server and a client.
	 * 
	 * @param client
	 *            the Socket object responsible for the communication with a
	 *            certain client
	 */
	private void serveClient(Socket client) {
		try {
			DataInputStream input;
			DataOutputStream output;
			if (secure) {
				//System.out.println("secure");
				SSLSocket clientSocket = (SSLSocket) client;
				// input stream
				input = new DataInputStream(clientSocket.getInputStream());
				// output stream
				output = new DataOutputStream(clientSocket.getOutputStream());
			} else {
				//System.out.println("unsecure");
				Socket clientSocket = client;
				// input stream
				input = new DataInputStream(clientSocket.getInputStream());
				// output stream
				output = new DataOutputStream(clientSocket.getOutputStream());
			}

			System.out.println(ip + ":" + String.valueOf(port) + " serving client");
			// JSON Parser
			JSONParser parser = new JSONParser();

			JSONObject request = null;
			String string;
			while (true) {
				request = (JSONObject) parser.parse(input.readUTF());
				// request = (JSONObject) parser.parse(string);
				if (debugON) {
					serverLog.fine("Received: " + request.toJSONString());
				}
				// System.out.println("Received request:" +
				// request.toJSONString());
				JSONArray reply = new JSONArray();
				parseRequest(reply, request, client);
				JSONObject replyObj = null;
				if (!reply.isEmpty()) {
					for (int i = 0; i < reply.size(); i++) {
						replyObj = (JSONObject) reply.get(i);
						if (debugON) {
							serverLog.log(Level.FINE, "SENT: {0}", replyObj.toJSONString());
						}
						output.writeUTF(replyObj.toString());
						output.flush();
						if (replyObj.containsKey("resourceSize")) {
							URI fileUri = new URI(replyObj.get("uri").toString());
							File f = new File(fileUri);
							// Start sending file
							RandomAccessFile byteFile = new RandomAccessFile(f, "r");
							// send 1 MB at a time
							byte[] sendingBuffer = new byte[1024 * 1024];
							int num;
							// While there are still bytes to send..
							while ((num = byteFile.read(sendingBuffer)) > 0) {
								output.write(Arrays.copyOf(sendingBuffer, num));
							}
							byteFile.close();
						}
					}
					// close the socket and break the while loop if it is not a
					// subscribe command
					if (!replyObj.containsKey("id")) {
						System.out.println("to close the socket");
						client.close();
						clientSockets.remove(client);
						break;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Parses the request received from a client
	 * 
	 * @param request
	 *            a JSONObject containing the request information
	 * @return a JSONArray that may contain one or more reply messages. Each
	 *         massage is encoded as a JSONObject
	 */
	private JSONArray parseRequest(JSONArray reply, JSONObject request, Socket client) {
		// TODO Auto-generated method stub
		JSONObject result = new JSONObject();

		if (!request.containsKey("command")) {
			result.put("response", "error");
			result.put("errorMessage", "missing or incorrect type for command");
			reply.add(result);

		} else {
			try {
				if (request.get("command").equals("PUBLISH")) {
					publishReply(request, result, reply);
				} else if (request.get("command").equals("REMOVE")) {
					removeReply(request, result, reply);
				} else if (request.get("command").equals("QUERY")) {
					queryReply(request, result, reply);
				} else if (request.get("command").equals("EXCHANGE")) {
					exchangeReply(request, result, reply);
				} else if (request.get("command").equals("SHARE")) {
					shareReply(request, result, reply);
				} else if (request.get("command").equals("FETCH")) {
					fetchReply(request, result, reply);
				} else if (request.get("command").equals("SUBSCRIBE")) {
					subscribeReply(request, result, reply, client);
				} else if (request.get("command").equals("UNSUBSCRIBE")) {
					unsubscribeReply(request, result, reply, client);
				} else {
					throw new Exception("invalid or unknown command");
				}
			} catch (Exception e) {
				e.printStackTrace();
				result.put("response", "error");
				result.put("errorMessage", e.getMessage());
				reply.add(result);
			}
		}
		return reply;
	}

	/**
	 * Reply a PUBLISH command
	 * 
	 * @param request
	 * @param result
	 * @param reply
	 * @throws Exception
	 */
	public void publishReply(JSONObject request, JSONObject result, JSONArray reply) throws Exception {
		if (!request.containsKey("resource")) {
			throw new Exception("missing resource");
		}
		JSONObject resourceObject = (JSONObject) request.get("resource");
		resourceManager.publish(resourceObject);

		result.put("response", "success");
		reply.add(result);
		// notify the subscribers of the new resource
		Thread t = new Thread(() -> {
			try {
				Resource rsc = new Resource(resourceObject);
				rsc.setEZserver(ip + ":" + String.valueOf(port));
				notifyAllSubscribers(rsc);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		t.start();
	}

	/**
	 * Reply a REMOVE command
	 * 
	 * @param request
	 * @param result
	 * @param reply
	 * @throws Exception
	 */
	public void removeReply(JSONObject request, JSONObject result, JSONArray reply) throws Exception {
		if (!request.containsKey("resource")) {
			throw new Exception("missing resource");
		}
		JSONObject resourceObject = (JSONObject) request.get("resource");
		resourceManager.remove(resourceObject);
		result.put("response", "success");
		reply.add(result);
	}

	/**
	 * Reply a QUERY command
	 * 
	 * @param request
	 * @param result
	 * @param reply
	 * @throws Exception
	 */
	public void queryReply(JSONObject request, JSONObject result, JSONArray reply) throws Exception {
		if (!request.containsKey("resourceTemplate")) {
			throw new Exception("missing resourceTemplate");
		}
		JSONObject template = (JSONObject) request.get("resourceTemplate");
		List<Resource> match = Collections.synchronizedList(resourceManager.query(template));
		result.put("response", "success");
		reply.add(result);
		// change the EZserver field of the resources matching the
		// template
		for (int i = 0; i < match.size(); i++) {
			Resource matchingResource = (Resource) match.get(i);
			matchingResource.setEZserver(ip + ":" + String.valueOf(port));
		}

		// If the relay field is true, send the QUERY command to each of
		// the servers in the records
		if ((boolean) request.get("relay")) {
			ExecutorService executorService = Executors.newFixedThreadPool(10);
			ArrayList<Future<Boolean>> futures = new ArrayList<Future<Boolean>>(serverRecords.size());
			synchronized (serverRecords) {
				for (String server : serverRecords) {
					// if (!server.equals(ip + ":" + String.valueOf(port))) {
					futures.add(
							executorService.submit(new QueryRelay(match, server, request, serverLog, secure), true));
					// }
				}
			}
			for (Future<Boolean> future : futures) {
				future.get();
			}
			executorService.shutdown();
		}
		for (int i = 0; i < match.size(); i++) {
			Resource matchingResource = (Resource) match.get(i);
			reply.add(matchingResource.toJSONObjHideOwner());
		}
		JSONObject resultSize = new JSONObject();
		resultSize.put("resultSize", match.size());
		reply.add(resultSize);
	}

	/**
	 * Reply a EXCHANGE command
	 * 
	 * @param request
	 * @param result
	 * @param reply
	 * @param client
	 * @throws Exception
	 */
	public void exchangeReply(JSONObject request, JSONObject result, JSONArray reply) throws Exception {
		if (!request.containsKey("serverList")) {
			throw new Exception("missing or invalid server list");
		}
		JSONArray serverList = (JSONArray) request.get("serverList");
		for (int i = 0; i < serverList.size(); i++) {
			JSONObject serverObj = (JSONObject) serverList.get(i);
			String aServerHostname = serverObj.get("hostname").toString();
			String aServerIp;
			if (aServerHostname.equals("localhost")) {
				aServerIp = ip;
			} else {
				aServerIp = InetAddress.getByName(aServerHostname).getHostAddress();
			}
			String aServerPort = serverObj.get("port").toString();
			String aServerStr = aServerIp + ":" + aServerPort;
			if (!aServerStr.equals(ip + ":" + String.valueOf(theOtherPort))
					&& !aServerStr.equals(ip + ":" + String.valueOf(port)) && !serverRecords.contains(aServerStr)) {
				
				serverRecords.add(aServerStr);
				// send existing subscriptions
				Thread t = new Thread(() -> {
					try {
						// System.out.println("relay");
						sendAllSubscriptions(aServerStr);
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
				t.start();
			}
		}
		result.put("response", "success");
		reply.add(result);
	}

	/**
	 * Reply a SHARE command
	 * 
	 * @param request
	 * @param result
	 * @param reply
	 * @throws Exception
	 */
	public void shareReply(JSONObject request, JSONObject result, JSONArray reply) throws Exception {
		if (!request.containsKey("resource") || !request.containsKey("secret")) {
			throw new Exception("missing resource and/or secret");
		}
		if (!request.get("secret").equals(secret)) {
			throw new Exception("incorrect secret");
		}
		JSONObject resourceObject = (JSONObject) request.get("resource");
		resourceManager.share(resourceObject);
		result.put("response", "success");
		reply.add(result);
		// notify the subscribers of the new resource
		Thread t = new Thread(() -> {
			try {
				Resource rsc = new Resource(resourceObject);
				rsc.setEZserver(ip + ":" + String.valueOf(port));
				notifyAllSubscribers(rsc);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		t.start();
	}

	/**
	 * Reply a FETCH command
	 * 
	 * @param request
	 * @param result
	 * @param reply
	 * @throws Exception
	 */
	public void fetchReply(JSONObject request, JSONObject result, JSONArray reply) throws Exception {
		if (!request.containsKey("resourceTemplate")) {
			throw new Exception("missing resourceTemplate");
		}
		JSONObject template = (JSONObject) request.get("resourceTemplate");
		Resource match = resourceManager.fetch(template);
		JSONObject matchObj = null;
		if (match != null) {
			URI fileUri = match.getUri();
			File f = new File(fileUri);
			// change the EZserver field
			match.setEZserver(ip + ":" + String.valueOf(port));
			matchObj = match.toJSONObjHideOwner();
			matchObj.put("resourceSize", f.length());
		}
		result.put("response", "success");
		reply.add(result);
		if (matchObj != null) {
			reply.add(matchObj);
		}

		JSONObject resultSize = new JSONObject();
		if (match != null) {
			resultSize.put("resultSize", 1);
		} else {
			resultSize.put("resultSize", 0);
		}
		reply.add(resultSize);
	}

	public void subscribeReply(JSONObject request, JSONObject result, JSONArray reply, Socket client) throws Exception {
		if (!request.containsKey("resourceTemplate")) {
			throw new Exception("missing resourceTemplate");
		}
		if (!request.containsKey("id")) {
			throw new Exception("missing id");
		}
		JSONObject template = (JSONObject) request.get("resourceTemplate");
		Subscriber subscriber = new Subscriber(request.get("id").toString(), template, (boolean) request.get("relay"));
		synchronized (clientSubscriptions) {
			if (clientSubscriptions.containsKey(client)) {
				clientSubscriptions.get(client).add(subscriber);
			} else {
				ArrayList<Subscriber> subscriberList = new ArrayList<Subscriber>();
				subscriberList.add(subscriber);
				clientSubscriptions.put(client, subscriberList);
			}
		}
		if ((boolean) request.get("relay") && serverRecords.size() > 0) {
			String relayId = clientSockets.get(client).toString() + request.get("id").toString();
			synchronized (serverRecords) {
				for (String server : serverRecords) {
					// check the server address
					if (!server.equals(ip + ":" + String.valueOf(port))) {
						if (outgoingConnections.containsKey(server)) {
							// output stream
							DataOutputStream output = new DataOutputStream(
									outgoingConnections.get(server).getOutputStream());
							JSONObject subscribeRelayRequest = new JSONObject();
							makeSubscribeRelayRequest(subscribeRelayRequest, subscriber.getSubscription(), relayId);
							output.writeUTF(subscribeRelayRequest.toJSONString());
							output.flush();
							if (debugON) {
								serverLog.log(Level.FINE, "SENT: {0}", subscribeRelayRequest.toJSONString());
							}
						} else {
							//System.out.println("subscription relay");
							Thread t = new Thread(() -> {
								try {
									subscriptionRelay(server, new ResourceTemplate(template), relayId);
								} catch (Exception e) {
									e.printStackTrace();
								}
							});
							t.start();
						}
					}

				}
			}

		}
		result.put("response", "success");
		result.put("id", request.get("id").toString());
		reply.add(result);
	}

	private void unsubscribeReply(JSONObject request, JSONObject result, JSONArray reply, Socket client)
			throws Exception {
		if (!request.containsKey("id")) {
			throw new Exception("missing id");
		}
		String subscriptionId = request.get("id").toString();
		synchronized (clientSubscriptions) {
			if (!clientSubscriptions.containsKey(client)) {
				throw new Exception("subscription record not found");
			}
		}

		String relayId = clientSockets.get(client).toString() + subscriptionId;
		JSONObject unsubscribeRequest = new JSONObject();
		unsubscribeRequest.put("command", "UNSUBSCRIBE");
		unsubscribeRequest.put("id", relayId);

		// remove the subscription from local list
		ArrayList<Subscriber> subscriberList = clientSubscriptions.get(client);
		Iterator<Subscriber> iterator = subscriberList.iterator();
		while (iterator.hasNext()) {
			Subscriber aSubscriber = iterator.next();
			if (subscriptionId.equals(aSubscriber.getId())) {
				if (aSubscriber.isRelay()) {
					// send unsubscribe requests to the other servers
					synchronized (serverRecords) {
						for (String server : serverRecords) {
							if (outgoingConnections.containsKey(server)) {
								// output stream
								DataOutputStream output = new DataOutputStream(
										outgoingConnections.get(server).getOutputStream());
								output.writeUTF(unsubscribeRequest.toJSONString());
								output.flush();
								if (debugON) {
									serverLog.log(Level.FINE, "SENT: {0}", unsubscribeRequest.toJSONString());
								}
							}
						}
					}
				}
				iterator.remove();
				break;
			}
		}
		// if all subscriptions from this client have stopped...
		if (subscriberList.isEmpty()) {
			int hits = clientSubscriptionHits.containsKey(client) ? clientSubscriptionHits.get(client) : 0;
			result.put("resultSize", hits);
			reply.add(result);
			clientSubscriptionHits.remove(client);
			clientSubscriptions.remove(client);
		}

	}

	public void notifyAllSubscribers(Resource rsc) {
		//rsc.setEZserver(ip + ":" + String.valueOf(port));
		synchronized (clientSubscriptions) {
			//System.out.println(clientSubscriptions.size());
			for (Socket subscriberSocket : clientSubscriptions.keySet()) {
				// System.out.println(subscriberSocket);
				ArrayList<Subscriber> subscriberList = clientSubscriptions.get(subscriberSocket);
				//System.out.println(subscriberList.size());
				for (Subscriber subscriber : subscriberList) {
					// System.out.println(subscriber.getSubscription());
					if (resourceManager.isHit(rsc, subscriber.getSubscription())) {
						pushResource(subscriberSocket, rsc);
						break;
					}
					// pushResource(subscriberSocket, rsc);
				}
			}
		}
	}

	private void pushResource(Socket subscriberSocket, Resource rsc) {
		try {
			// output stream
			DataOutputStream output = new DataOutputStream(subscriberSocket.getOutputStream());
			String message = rsc.toJSONObjHideOwner().toJSONString();
			//System.out.println(message);
			output.writeUTF(message);
			output.flush();
			// System.out.println("here");
			if (debugON) {
				serverLog.log(Level.FINE, "SENT: {0}", message);
			}
			int hits = clientSubscriptionHits.containsKey(subscriberSocket)
					? clientSubscriptionHits.get(subscriberSocket) : 0;
			clientSubscriptionHits.put(subscriberSocket, hits + 1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void subscriptionRelay(String relayServer, ResourceTemplate subscription, String relayId) {
		String serverHost = relayServer.split(":")[0];
		int serverPort = Integer.parseInt(relayServer.split(":")[1]);
		Socket socket = null;
		DataOutputStream output = null;
		DataInputStream input = null;
		try {
			if (!secure) {
				socket = new Socket(serverHost, serverPort);
			} else {
				SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
				socket = (SSLSocket) sslsocketfactory.createSocket(serverHost, serverPort);
			}
			// socket.setSoTimeout(10 * 1000);
			outgoingConnections.put(relayServer, socket);
			output = new DataOutputStream(socket.getOutputStream());
			input = new DataInputStream(socket.getInputStream());
			// send a subscribe request
			JSONObject subscribeRelayRequest = new JSONObject();
			makeSubscribeRelayRequest(subscribeRelayRequest, subscription, relayId);
			output.writeUTF(subscribeRelayRequest.toJSONString());
			output.flush();
			if (debugON) {
				serverLog.log(Level.FINE, "SENT: {0}", subscribeRelayRequest.toJSONString());
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			serverRecords.remove(relayServer);
			System.out.println("Unreachable server: removed from server record.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			serverRecords.remove(relayServer);
			System.out.println("Unreachable server: removed from server record.");
		}
		// receive hits from the other server and push to the client
		JSONParser jsonParser = new JSONParser();
		String message = null;
		while (true) {
			try {
				message = input.readUTF();
				if (debugON) {
					serverLog.fine("RECEIVED: " + message);
				}
				// notify the subscribers
				JSONObject msgObj = null;
				try {
					msgObj = (JSONObject) jsonParser.parse(message);
					//System.out.println(msgObj);
					if (msgObj.containsKey("uri")) {
						Resource aResource = null;
						try {
							aResource = new Resource(msgObj,true);
							aResource.setEZserver(msgObj.get("ezserver").toString());						
							notifyAllSubscribers(aResource);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
			}

		}

	}

	private void makeSubscribeRelayRequest(JSONObject subscribeRelayRequest, ResourceTemplate subscription,
			String relayId) {
		subscribeRelayRequest.put("command", "SUBSCRIBE");
		subscribeRelayRequest.put("relay", false);
		subscribeRelayRequest.put("id", relayId);
		JSONObject subscribeRelayTemplate = new JSONObject();
		subscribeRelayTemplate.put("name", subscription.getName());
		subscribeRelayTemplate.put("description", subscription.getDescription());
		subscribeRelayTemplate.put("tags", subscription.getTags());
		if (!subscription.getUri().toString().equals("")) {
			subscribeRelayTemplate.put("uri", subscription.getUri().toString());
		}
		subscribeRelayTemplate.put("channel", "");
		subscribeRelayTemplate.put("owner", "");
		subscribeRelayRequest.put("resourceTemplate", subscribeRelayTemplate);
	}

	private void sendAllSubscriptions(String relayServer) {
		//System.out.println("send all subscriptions");
		String serverHost = relayServer.split(":")[0];
		int serverPort = Integer.parseInt(relayServer.split(":")[1]);
		Socket socket = null;
		DataOutputStream output = null;
		DataInputStream input = null;
		try {
			if (!clientSubscriptions.isEmpty()) {
				if (!secure) {
					socket = new Socket(serverHost, serverPort);
				} else {
					SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
					socket = (SSLSocket) sslsocketfactory.createSocket(serverHost, serverPort);
				}
				//System.out.println("socket created");
				// socket.setSoTimeout(10 * 1000);
				outgoingConnections.put(relayServer, socket);
				output = new DataOutputStream(socket.getOutputStream());
				input = new DataInputStream(socket.getInputStream());
				// send subscribe requests
				synchronized (clientSubscriptions) {
					for (Socket subscriberSocket : clientSubscriptions.keySet()) {
						String relayIdPrefix = String.valueOf(clientSockets.get(subscriberSocket));
						ArrayList<Subscriber> subscriberList = clientSubscriptions.get(subscriberSocket);
						for (Subscriber subscriber : subscriberList) {
							if (subscriber.isRelay()) {
								String relayId = relayIdPrefix + subscriber.getId();
								JSONObject subscribeRelayRequest = new JSONObject();
								makeSubscribeRelayRequest(subscribeRelayRequest, subscriber.getSubscription(), relayId);
								output.writeUTF(subscribeRelayRequest.toJSONString());
								output.flush();
								if (debugON) {
									serverLog.log(Level.FINE, "SENT: {0}", subscribeRelayRequest.toJSONString());
								}
							}
						}
					}
				}
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			serverRecords.remove(relayServer);
			System.out.println("Unreachable server: removed from server record.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			serverRecords.remove(relayServer);
			System.out.println("Unreachable server: removed from server record.");
		}

		// receive hits from the other server and push to the client
		JSONParser jsonParser = new JSONParser();
		String message = null;
		if (socket != null) {
			while (true) {
				try {
					message = input.readUTF();
					if (debugON) {
						serverLog.fine("RECEIVED: " + message);
					}
					// notify the subscribers
					JSONObject msgObj = null;
					try {
						msgObj = (JSONObject) jsonParser.parse(message);
						if (msgObj.containsKey("uri")) {
							Resource aResource = null;
							try {
								aResource = new Resource(msgObj, true);
								aResource.setEZserver(msgObj.get("ezserver").toString());
								notifyAllSubscribers(aResource);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
			}
		}

	}
}
