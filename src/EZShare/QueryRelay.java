package EZShare;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.Logger;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * This class implements a runnable task that propagates a query command from
 * one server to others.
 * 
 * @author yuxin
 *
 */
public class QueryRelay implements Runnable {
	private List<Resource> match;
	private String aServer;
	private JSONObject request;
	private Logger logger;
	private boolean secure;

	/**
	 * Constructs a QueryRelay object.
	 * 
	 * @param match
	 *            a list of resources matching the template specified in the
	 *            request
	 * @param aServer
	 *            hostname:port of a server
	 * @param request
	 *            a JSONObject instance representing a query request
	 * @param logger
	 *            the Logger object of the original server
	 */
	public QueryRelay(List<Resource> match, String aServer, JSONObject request, Logger logger, boolean secure) {
		super();
		this.match = match;
		this.aServer = aServer;
		this.logger = logger;
		this.secure = secure;
		this.request = request;
		// set the owner and channel to be "" in the forwarded query
		this.request.put("owner", "");
		this.request.put("channel", "");
		// set the relay field to be false
		this.request.put("relay", false);
	}

	@Override
	public void run() {
		Socket socket = null;
		DataOutputStream output = null;
		DataInputStream input = null;
		String serverHostname = aServer.split(":")[0];
		int serverPort = Integer.parseInt(aServer.split(":")[1]);
		try {
			if (!secure) {
				socket = new Socket(serverHostname, serverPort);
			} else {
				SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
				socket = (SSLSocket) sslsocketfactory.createSocket(serverHostname, serverPort);
			}
			socket.setSoTimeout(10 * 1000);
			// outgoingConnections.put(randomServer, socket);
			output = new DataOutputStream(socket.getOutputStream());
			input = new DataInputStream(socket.getInputStream());
			logger.fine("SENT TO " + aServer + ": " + request.toJSONString());
			output.writeUTF(request.toJSONString());
			output.flush();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// receive response from the server
		JSONParser parser = new JSONParser();
		if (socket != null) {
			while (true) {
				String message = null;
				try {
					message = input.readUTF();
					logger.fine("RECEIVED: " + message);
					// System.out.println("RECEIVED: " + message);
					JSONObject msgObj = null;
					try {
						msgObj = (JSONObject) parser.parse(message);
						if (msgObj.containsKey("uri")) {
							Resource matchingResource = null;
							try {
								matchingResource = new Resource(msgObj, true);
								matchingResource.setEZserver(aServer);
								match.add(matchingResource);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						if (msgObj.containsKey("resultSize")) {
							break;
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
