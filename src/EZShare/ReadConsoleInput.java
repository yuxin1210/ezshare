package EZShare;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

public class ReadConsoleInput implements Runnable{
	private DataOutputStream output;
	private boolean debugON;
	private Logger logger;
	
	
	public ReadConsoleInput(DataOutputStream output, boolean debugON, Logger logger) {
		super();
		this.output = output;
		this.debugON = debugON;
		this.logger = logger;
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		InputStream inputstream = System.in;
		InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
		BufferedReader bufferedreader = new BufferedReader(inputstreamreader);
		String consoleInput;
		try {
			consoleInput = bufferedreader.readLine();
			System.out.println(consoleInput);
			if (consoleInput.equals("")) {
				JSONObject request = new JSONObject();
				Client.unsubscribeRequest(request);
				// write JSON to server
				output.writeUTF(request.toJSONString());
				output.flush();
				if (debugON) {
					logger.fine("SENT: " + request.toJSONString());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
