package EZShare;

import java.net.URI;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This class implements the resources in EZShare, a resource sharing network.
 * In general, resources can be stored in servers as references.
 * 
 * @author yuxin
 *
 */
public class Resource {
	private String name = "";
	private String description = "";
	private JSONArray tags = new JSONArray();
	private URI uri;
	private String channel = "";
	private String owner = "";
	private String EZserver = null;

	/**
	 * A method to construct a Resource instance from a JSONObject.
	 * 
	 * @param resource
	 *            a JSONObject instance containing the information of a resource
	 * @throws Exception
	 */
	public Resource(JSONObject resource) throws Exception {
		try {
			this.uri = new URI(resource.get("uri").toString());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("invalid resource");
		}

		if (!this.uri.isAbsolute()) {
			throw new Exception("invalid resource");
		}
		if (resource.containsKey("channel")) {
			this.setChannel(resource.get("channel").toString());
		}
		if (resource.containsKey("owner")) {
			this.setOwner(resource.get("owner").toString());
		}
		if (resource.containsKey("name")) {
			this.setName(resource.get("name").toString());
		}
		if (resource.containsKey("tags")) {
			this.tags = (JSONArray) resource.get("tags");
		}
		if (resource.containsKey("description")) {
			this.setDescription(resource.get("description").toString());
		}
	}

	/**
	 * A method to construct a Resource instance from a JSONObject.
	 * 
	 * @param resource
	 *            a JSONObject instance containing the information of a resource
	 * @param ownerHided
	 *            If this argument is true, the owner field should be masked in
	 *            the JSONObject. An exception will not be thrown when the owner
	 *            field is "*".
	 * @throws Exception
	 */
	public Resource(JSONObject resource, boolean ownerHided) throws Exception {
		try {
			this.uri = new URI(resource.get("uri").toString());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("missing resource");
		}

		if (!this.uri.isAbsolute()) {
			throw new Exception("invalid resource");
		}
		if (resource.containsKey("channel")) {
			this.setChannel(resource.get("channel").toString());
		}
		if (resource.containsKey("owner")) {
			if (ownerHided) {
				this.setOwnerHided(resource.get("owner").toString());
			} else {
				this.setOwner(resource.get("owner").toString());
			}
		}
		if (resource.containsKey("name")) {
			this.setName(resource.get("name").toString());
		}
		if (resource.containsKey("tags")) {
			this.tags = (JSONArray) resource.get("tags");
		}
		if (resource.containsKey("description")) {
			this.setDescription(resource.get("description").toString());
		}
	}

	/**
	 * A method that converts an instance of Resource to a JSONObject
	 * 
	 * @return the corresponding JSONObject of this resource instance.
	 */
	public JSONObject toJSONObj() {
		JSONObject resource_json = new JSONObject();
		resource_json.put("name", name);
		resource_json.put("description", description);
		resource_json.put("tags", tags);
		resource_json.put("uri", uri.toString());
		resource_json.put("channel", channel);
		resource_json.put("owner", owner);
		resource_json.put("ezserver", EZserver);
		return resource_json;
	}

	/**
	 * A method that converts an instance of Resource to a JSONObject, but hides
	 * the owner of the resource if exists
	 * 
	 * @return the corresponding JSONObject of this resource instance.
	 */

	public JSONObject toJSONObjHideOwner() {
		JSONObject resource_json = new JSONObject();
		resource_json.put("name", name);
		resource_json.put("description", description);
		resource_json.put("tags", tags);
		resource_json.put("uri", uri.toString());
		resource_json.put("channel", channel);
		if (!owner.equals("")) {
			resource_json.put("owner", "*");
		} else {
			resource_json.put("owner", owner);
		}
		resource_json.put("ezserver", EZserver);
		return resource_json;
	}

	@Override
	public String toString() {
		return "Resource [name=" + name + ", description=" + description + ", tags=" + tags + ", uri=" + uri
				+ ", channel=" + channel + ", owner=" + owner + ", EZserver=" + EZserver + "]";
	}

	/**
	 * Returns the name of the resource.
	 * 
	 * @return the name of the resource
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the description of the resource.
	 * 
	 * @return the description of the resource.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the tags of the resource.
	 * 
	 * @return the tags of the resource.
	 */
	public JSONArray getTags() {
		return tags;
	}

	/**
	 * Returns the URI of the resource.
	 * 
	 * @return the URI of the resource.
	 */
	public URI getUri() {
		return uri;
	}

	/**
	 * Returns the channel of the resource.
	 * 
	 * @return the channel of the resource.
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * Returns the owner of the resource.
	 * 
	 * @return the owner of the resource.
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * Returns the server that lists the resource.
	 * 
	 * @return the server that lists the resource.
	 */
	public String getEZserver() {
		return EZserver;
	}

	/**
	 * Sets the name of the resource. The '\0' character, leading and ending
	 * whitespace in the string are removed.
	 * 
	 * @param name
	 *            the name of the resource.
	 */
	public void setName(String name) {
		this.name = name;
		this.name = this.name.replace("\\0", "");
		this.name = this.name.trim();
	}

	/**
	 * Sets the description of the resource. The '\0' character, leading and
	 * ending whitespace in the string are removed.
	 * 
	 * @param description
	 *            the description of the resource.
	 */
	public void setDescription(String description) {
		this.description = description;
		this.description = this.description.replace("\\0", "");
		this.description = this.description.trim();
	}

	/**
	 * Sets the tags of the resource.
	 * 
	 * @param tags
	 *            the tags of the resource.
	 */
	public void setTags(JSONArray tags) {
		this.tags = tags;
	}

	/**
	 * Sets the URI of the resource.
	 * 
	 * @param uri
	 *            the URI of the resource.
	 */
	public void setUri(URI uri) {
		this.uri = uri;
	}

	/**
	 * Sets the channel of the resource. The '\0' character, leading and ending
	 * whitespace in the string are removed.
	 * 
	 * @param channel
	 *            the channel of the resource
	 */
	public void setChannel(String channel) {
		this.channel = channel;
		this.channel = this.channel.replace("\\0", "");
		this.channel = this.channel.trim();
	}

	/**
	 * Sets the owner of the resource. The '\0' character, leading and ending
	 * whitespace in the string are removed. Throws an exception is the owner
	 * field is the single character "*".
	 * 
	 * @param owner
	 *            the owner of the resource
	 * @throws Exception
	 */
	public void setOwner(String owner) throws Exception {
		String ownerField = owner.replace("\\0", "");
		ownerField = ownerField.trim();
		if (ownerField.equals("*")) {
			throw new Exception("invalid resource");
		} else {
			this.owner = ownerField;
		}
	}

	/**
	 * Sets the owner of the resource. The '\0' character, leading and ending
	 * whitespace in the string are removed.
	 * 
	 * @param owner
	 *            the owner of the resource
	 */
	public void setOwnerHided(String owner) {
		String ownerField = owner.replace("\\0", "");
		ownerField = ownerField.trim();
		this.owner = ownerField;
	}

	/**
	 * Sets the server that lists the resource.
	 * 
	 * @param eZserver
	 *            the server that lists the resource
	 */
	public void setEZserver(String eZserver) {
		EZserver = eZserver;
	}

}
