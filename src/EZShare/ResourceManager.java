package EZShare;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This class implements a resource manager, each associated with a server in a
 * resource sharing network. A resource manager is in charge of adding, removing
 * and querying the resources that are listed on the server.
 *
 * @author yuxin
 *
 */
public class ResourceManager {
	private ConcurrentHashMap<Tuple<String>, Resource> resourceMap;

	/**
	 * Constructs a ResourceManager instance for a server.
	 *
	 * @param resourceMap
	 *            a ConcurrentHashMap that stores the resource objects
	 */
	public ResourceManager(ConcurrentHashMap<Tuple<String>, Resource> resourceMap) {
		this.resourceMap = resourceMap;
	}

	/**
	 * A method that publishes a resource to the server
	 *
	 * @param resourceObject
	 *            a JSONObject instance containing the information of the
	 *            resource to be published
	 * @throws Exception
	 */
	public void publish(JSONObject resourceObject) throws Exception {
		Resource aResource = null;
		try {
			aResource = new Resource(resourceObject);
			if (aResource.getUri().getScheme().equals("file")) {
				throw new Exception("invalid resource");
			}
			Tuple<String> pk = new Tuple<String>(aResource.getOwner(), aResource.getChannel(),
					aResource.getUri().toString());

			for (Tuple<String> key : resourceMap.keySet()) {
				String owner = key.getFirst();
				String channel = key.getSecond();
				String uristr = key.getThird();
				if (pk.getSecond().equals(channel) && pk.getThird().equals(uristr) && !pk.getFirst().equals(owner)) {
					throw new Exception("cannot publish resource");
				}
			}
			// System.out.println(aResource.toString());
			resourceMap.put(pk, aResource);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * A method that shares a file on the server machine and allows it to be
	 * fetched by clients.
	 *
	 * @param resourceObject
	 *            a JSONObject instance containing the information of the file
	 *            resource to be shared
	 * @throws Exception
	 */
	public void share(JSONObject resourceObject) throws Exception {
		Resource aResource = null;
		try {
			aResource = new Resource(resourceObject);
			// System.out.println(aResource.getUri().getScheme());
			if (!aResource.getUri().getScheme().equals("file")) {
				throw new Exception("invalid resource");
			}
			File f = new File(aResource.getUri());
			// check if file exists
			if (!f.exists()) {
				throw new Exception("invalid resource");
			}
			Tuple<String> pk = new Tuple<String>(aResource.getOwner(), aResource.getChannel(),
					aResource.getUri().toString());
			for (Tuple<String> key : resourceMap.keySet()) {
				String owner = key.getFirst();
				String channel = key.getSecond();
				String uristr = key.getThird();
				if (pk.getSecond().equals(channel) && pk.getThird().equals(uristr) && !pk.getFirst().equals(owner)) {
					throw new Exception("cannot share resource");
				}
			}
			// System.out.println(aResource.toString());
			resourceMap.put(pk, aResource);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * A method that removes a resource from the server
	 *
	 * @param resourceObject
	 *            a JSONObject instance containing the information of the
	 *            resource that needs to be removed
	 * @throws Exception
	 */
	public void remove(JSONObject resourceObject) throws Exception {
		Resource aResource = null;
		aResource = new Resource(resourceObject);
		Tuple<String> pk = new Tuple<String>(aResource.getOwner(), aResource.getChannel(),
				aResource.getUri().toString());
		if (resourceMap.containsKey(pk)) {
			resourceMap.remove(pk);
		} else {
			throw new Exception("cannot remove resource");
		}

	}

	/**
	 * A method that return a list of resources that match a template.
	 *
	 * @param template
	 *            a resource template to be matched against
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Resource> query(JSONObject template) throws Exception {
		ResourceTemplate resourceTemplate = new ResourceTemplate(template);
		ArrayList<Resource> match = new ArrayList<Resource>();

		String templateChannel = resourceTemplate.getChannel();
		String templateOwner = resourceTemplate.getOwner();
		JSONArray templateTags = resourceTemplate.getTags();
		URI templateUri = resourceTemplate.getUri();
		String templateName = resourceTemplate.getName();
		String templateDescription = resourceTemplate.getDescription();

		out: for (Resource rsc : resourceMap.values()) {
			if (templateChannel.equals(rsc.getChannel())) {
				if (templateOwner.equals("")
						|| (!templateOwner.equals("") && resourceTemplate.getOwner().equals(rsc.getOwner()))) {
					JSONArray candidateTags = rsc.getTags();
					for (int i = 0; i < templateTags.size(); i++) {
						if (!candidateTags.contains(templateTags.get(i).toString())) {
							// The current resource is not a match, go to next
							// outer loop iteration
							continue out;
						}
					}
					if (templateUri.toString().equals("") || (templateUri.toString().equals("")
							&& templateUri.toString().equals(rsc.getUri().toString()))) {
						if ((rsc.getName().contains(templateName) && !templateName.equals(""))
								|| (rsc.getDescription().contains(templateDescription)
										&& !templateDescription.equals(""))
								|| (templateName.equals("") && templateDescription.equals(""))) {
							// match.add(rsc.toJSONObjHideOwner());
							match.add(rsc);
						}
					}
				}
			}
		}

		return match;
	}

	/**
	 * A method that determines if a resource matches a template
	 * 
	 * @param rsc
	 *            A Resource object
	 * @param resourceTemplate
	 *            A ResourceTemplate object
	 * @return Returns true if the resource matches the template.
	 */
	public boolean isHit(Resource rsc, ResourceTemplate resourceTemplate) {
		String templateChannel = resourceTemplate.getChannel();
		String templateOwner = resourceTemplate.getOwner();
		JSONArray templateTags = resourceTemplate.getTags();
		URI templateUri = resourceTemplate.getUri();
		String templateName = resourceTemplate.getName();
		String templateDescription = resourceTemplate.getDescription();

		if (templateChannel.equals(rsc.getChannel())) {
			if (templateOwner.equals("")
					|| (!templateOwner.equals("") && resourceTemplate.getOwner().equals(rsc.getOwner()))) {
				JSONArray candidateTags = rsc.getTags();
				for (int i = 0; i < templateTags.size(); i++) {
					if (!candidateTags.contains(templateTags.get(i).toString())) {
						return false;
					}
				}
				if (templateUri.toString().equals("") || (templateUri.toString().equals("")
						&& templateUri.toString().equals(rsc.getUri().toString()))) {
					if ((rsc.getName().contains(templateName) && !templateName.equals(""))
							|| (rsc.getDescription().contains(templateDescription) && !templateDescription.equals(""))
							|| (templateName.equals("") && templateDescription.equals(""))) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * A method that returns the file resource that matches the template
	 *
	 * @param template
	 *            a resource template to be matched
	 * @return a resource that matches the template against
	 * @throws Exception
	 */
	public Resource fetch(JSONObject template) throws Exception {
		Resource resourceTemplate = new Resource(template);
		URI templateUri = resourceTemplate.getUri();
		if (!templateUri.getScheme().equals("file")) {
			throw new Exception("invalid resourceTemplate");
		}
		String templateChannel = resourceTemplate.getChannel();
		Resource match = null;
		for (Tuple<String> key : resourceMap.keySet()) {
			String channel = key.getSecond();
			String uristr = key.getThird();
			if (templateChannel.equals(channel) && templateUri.toString().equals(uristr)) {
				match = resourceMap.get(key);
				break;
			}
		}
		return match;
	}
}
