package EZShare;

import java.net.URI;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This class implements the resource templates in EZShare, a resource sharing
 * network. The implementation is very similar to the Resource class. The main
 * difference is that a URI is not required to initiate an instance of a
 * resource template.
 * 
 * @author yuxin
 *
 */
public class ResourceTemplate {
	private String name = "";
	private String description = "";
	private JSONArray tags = new JSONArray();
	private URI uri = new URI("");
	private String channel = "";
	private String owner = "";
	private String EZserver = null;

	/**
	 * A method to construct a ResourceTemplate instance from a JSONObject.
	 * 
	 * @param template
	 *            a JSONObject instance containing the information of a resource
	 *            template
	 * @throws Exception
	 */
	public ResourceTemplate(JSONObject template) throws Exception {
		// System.out.println(template);
		if (template.containsKey("uri")) {
			this.uri = new URI(template.get("uri").toString());
			if (!this.uri.isAbsolute()) {
				throw new Exception("invalid resourceTemplate");
			}
		}
		if (template.containsKey("channel")) {
			this.setChannel(template.get("channel").toString());
		}
		if (template.containsKey("owner")) {
			this.setOwner(template.get("owner").toString());
		}
		if (template.containsKey("name")) {
			this.setName(template.get("name").toString());
		}
		if (template.containsKey("tags")) {
			this.tags = (JSONArray) template.get("tags");
		}
		if (template.containsKey("description")) {
			this.setDescription(template.get("description").toString());
		}

	}

	/**
	 * Sets the name of the resource template. The '\0' character, leading and
	 * ending whitespace in the string are removed.
	 * 
	 * @param name
	 *            the name of the resource template.
	 */
	public void setName(String name) {
		this.name = name;
		this.name = this.name.replace("\\0", "");
		this.name = this.name.trim();
	}

	/**
	 * Sets the description of the resource template. The '\0' character,
	 * leading and ending whitespace in the string are removed.
	 * 
	 * @param description
	 *            the description of the resource template.
	 */
	public void setDescription(String description) {
		this.description = description;
		this.description = this.description.replace("\\0", "");
		this.description = this.description.trim();
	}

	/**
	 * Sets the tags of the resource template.
	 * 
	 * @param tags
	 *            the tags of the resource template.
	 */
	public void setTags(JSONArray tags) {
		this.tags = tags;
	}

	/**
	 * Sets the URI of the resource template.
	 * 
	 * @param uri
	 *            the URI of the resource template.
	 */
	public void setUri(URI uri) {
		this.uri = uri;
	}

	/**
	 * Sets the channel of the resource template. The '\0' character, leading
	 * and ending whitespace in the string are removed.
	 * 
	 * @param channel
	 *            the channel of the resource template
	 */
	public void setChannel(String channel) {
		this.channel = channel;
		this.channel = this.channel.replace("\\0", "");
		this.channel = this.channel.trim();
	}

	/**
	 * Sets the owner of the resource template. The '\0' character, leading and
	 * ending whitespace in the string are removed. Throws an exception is the
	 * owner field is the single character "*".
	 * 
	 * @param owner
	 *            the owner of the resource template
	 * @throws Exception
	 */
	public void setOwner(String owner) throws Exception {
		String ownerField = owner.replace("\\0", "");
		ownerField = ownerField.trim();
		if (ownerField.equals("*")) {
			throw new Exception("invalid resourceTemplate");
		} else {
			this.owner = ownerField;
		}
	}

	/**
	 * Sets the server that lists the resource template.
	 * 
	 * @param eZserver
	 *            the server that lists the resource template
	 */
	public void setEZserver(String eZserver) {
		EZserver = eZserver;
	}

	/**
	 * Returns the name of the resource template.
	 * 
	 * @return the name of the resource template
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the description of the resource template.
	 * 
	 * @return the description of the resource template.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the tags of the resource template.
	 * 
	 * @return the tags of the resource template.
	 */
	public JSONArray getTags() {
		return tags;
	}

	/**
	 * Returns the URI of the resource template.
	 * 
	 * @return the URI of the resource template.
	 */
	public URI getUri() {
		return uri;
	}

	/**
	 * Returns the channel of the resource template.
	 * 
	 * @return the channel of the resource template.
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * Returns the owner of the resource template.
	 * 
	 * @return the owner of the resource template.
	 */
	public String getOwner() {
		return owner;
	}

	public String getEZserver() {
		return EZserver;
	}

	@Override
	public String toString() {
		return "ResourceTemplate [name=" + name + ", description=" + description + ", tags=" + tags + ", uri=" + uri
				+ ", channel=" + channel + ", owner=" + owner + ", EZserver=" + EZserver + "]";
	}

	/**
	 * A method that converts an instance of ResourceTemplate to a JSONObject
	 * 
	 * @return the corresponding JSONObject of this ResourceTemplate instance.
	 */
	public JSONObject toJSONObj() {
		JSONObject resource_json = new JSONObject();
		resource_json.put("name", name);
		resource_json.put("description", description);
		resource_json.put("tags", tags);
		resource_json.put("uri", uri);
		resource_json.put("channel", channel);
		resource_json.put("owner", owner);
		resource_json.put("ezserver", EZserver);
		return resource_json;
	}

}
