package EZShare;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.net.UnknownHostException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class implements the servers in EZShare, a resource sharing network. A
 * server can communicate with clients as well as other servers. Each server
 * maintains a list of resources that are published or shared to it. Servers can
 * be queried for the resources on the list.
 * 
 * @author yuxin
 *
 */
public class Server {
	private static String ip;
	private static String advertisedHostname = null;
	private static int port = 3000;
	private static int sport = 3781;
	// The default exchange interval is 10 minutes
	public static int exchangeInterval = 600;
	// The default connection interval limit is 1 second
	private static int connectionIntervalLimit = 1;
	// create Options object
	private static Options options = new Options();
	// a ConcurrentHashMap to store the resources
	private static final ConcurrentHashMap<Tuple<String>, Resource> resourceMap = new ConcurrentHashMap<Tuple<String>, Resource>();
	// a ResourceManager instance
	private static ResourceManager resourceManager = new ResourceManager(resourceMap);
	// a list of server records
	private static List<String> serverRecords = Collections.synchronizedList(new ArrayList<String>());
	private static List<String> secureServerRecords = Collections.synchronizedList(new ArrayList<String>());
	// The server secret
	private static String secret = "abcd1234";
	// Server's global logger
	private static final Logger serverLog = Logger.getGlobal();
	private static boolean debugON = false;
	// a list of subscribers
	// private static List<Subscriber> subscribers =
	// Collections.synchronizedList(new ArrayList<Subscriber>());

	public static void main(String[] args) {

		// Specify the keystore details (this can be specified as VM arguments
		// as well)
		// the keystore file contains an application's own certificate and
		// private key
		// System.setProperty("javax.net.ssl.keyStore",
		// "classes/serverKeystore.jks");
		// // Password to access the private key from the keystore file
		// System.setProperty("javax.net.ssl.keyStorePassword", "comp90015");
		// System.setProperty("javax.net.ssl.trustStore",
		// "classes/clientKeystore.jks");
		// System.setProperty("javax.net.ssl.trustStorePassword", "comp90015");
		// Enable debugging to view the handshake and communication which
		// happens between the SSLClient and the SSLServer
		// System.setProperty("javax.net.debug", "all");

		InputStream keystoreInput = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("serverKeystore.jks");
		InputStream truststoreInput = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("clientKeystore.jks");
		try {
			setSSLFactories(keystoreInput, "comp90015", truststoreInput);
		} catch (Exception e3) {
			e3.printStackTrace();
		}
		try {
			keystoreInput.close();
			truststoreInput.close();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		try {
			ip = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}

		serverLog.info("Starting the EZShare Server");

		// add options
		options.addOption("advertisedhostname", true, "advertised hostname");
		options.addOption("connectionintervallimit", true, "connection interval limit in seconds");
		options.addOption("exchangeinterval", true, "exchange interval in seconds");
		options.addOption("port", true, "server port, an integer");
		options.addOption("secret", true, "secret");
		options.addOption("debug", false, "print debug information");
		options.addOption("sport", true, "secure port number, an integer");

		// a command line parser
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;
		// Console handler used for printing out logger messages
		Handler consoleHandler = new ConsoleHandler();
		consoleHandler.setLevel(Level.FINE);
		serverLog.addHandler(consoleHandler);
		// Prevent logs from processed by default Console handler.
		serverLog.setUseParentHandlers(false);
		serverLog.setLevel(Level.FINE);

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// parse command line arguments
		if (cmd.hasOption("secret")) {
			secret = cmd.getOptionValue("secret");
		}
		serverLog.info("using secret: " + secret);
		if (cmd.hasOption("advertisedhostname")) {
			advertisedHostname = cmd.getOptionValue("advertisedhostname");
			serverLog.info("using advertised hostname: " + advertisedHostname);
		}
		if (cmd.hasOption("port")) {
			port = Integer.parseInt(cmd.getOptionValue("port"));
		}
		if (cmd.hasOption("sport")) {
			sport = Integer.parseInt(cmd.getOptionValue("sport"));
		}
		serverLog.info("bound to port " + String.valueOf(port));
		if (cmd.hasOption("exchangeinterval")) {
			exchangeInterval = Integer.parseInt(cmd.getOptionValue("exchangeinterval"));
		}
		if (cmd.hasOption("connectionintervallimit")) {
			connectionIntervalLimit = Integer.parseInt(cmd.getOptionValue("connectionintervallimit"));
		}

		if (cmd.hasOption("debug")) {
			debugON = true;
			serverLog.info("setting debug on");
		}
		serverLog.info("started");

		ServerSocketFactory factory = ServerSocketFactory.getDefault();
		SSLServerSocketFactory sslserversocketfactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();

		try {
			ServerSocket server = factory.createServerSocket(port);
			SSLServerSocket sslserversocket = (SSLServerSocket) sslserversocketfactory.createServerSocket(sport);
			MultiThreadedServer unsecuredSocket = new MultiThreadedServer(server, ip, sport, resourceManager,
					connectionIntervalLimit, secret, debugON, serverRecords, serverLog, false);
			new Thread(unsecuredSocket).start();
			MultiThreadedServer secureSocket = new MultiThreadedServer(sslserversocket, ip, port, resourceManager,
					connectionIntervalLimit, secret, debugON, secureServerRecords, serverLog, true);
			new Thread(secureSocket).start();

			// exchange of unsecured server lists
			// ServerExchangeTask serverExchangeTask = new ServerExchangeTask(ip
			// + ":" + String.valueOf(port),
			// exchangeInterval, serverRecords, serverLog, false);
			// Thread serverExchangeThread = new Thread(serverExchangeTask);
			// serverExchangeThread.start();
			// Thread.sleep(1 * 1000);
			// // exchange of secure server lists
			// ServerExchangeTask secureServerExchangeTask = new
			// ServerExchangeTask(ip + ":" + String.valueOf(sport),
			// exchangeInterval, secureServerRecords, serverLog, true);
			// Thread secureServerExchangeThread = new
			// Thread(secureServerExchangeTask);
			// secureServerExchangeThread.start();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void setSSLFactories(InputStream keyStream, String keyStorePassword, InputStream trustStream)
			throws Exception {
		// Get keyStore
		KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

		// if your store is password protected then declare it (it can be null
		// however)
		char[] keyPassword = keyStorePassword.toCharArray();

		// load the stream to your store
		keyStore.load(keyStream, keyPassword);

		// initialize a trust manager factory with the trusted store
		KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		keyFactory.init(keyStore, keyPassword);

		// get the trust managers from the factory
		KeyManager[] keyManagers = keyFactory.getKeyManagers();

		// Now get trustStore
		KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());

		// if your store is password protected then declare it (it can be null
		// however)
		// char[] trustPassword = password.toCharArray();

		// load the stream to your store
		trustStore.load(trustStream, keyPassword);

		// initialize a trust manager factory with the trusted store
		TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustFactory.init(trustStore);

		// get the trust managers from the factory
		TrustManager[] trustManagers = trustFactory.getTrustManagers();

		// initialize an ssl context to use these managers and set as default
		SSLContext sslContext = SSLContext.getInstance("SSL");
		sslContext.init(keyManagers, trustManagers, null);
		SSLContext.setDefault(sslContext);
	}

}