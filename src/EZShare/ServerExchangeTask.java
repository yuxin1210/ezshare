package EZShare;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * This class implements a runnable task that allows a server to exchange its
 * server records with other servers.
 * 
 * @author yuxin
 *
 */
public class ServerExchangeTask implements Runnable {
	private String selfip;
	private int exchangeInterval;
	private List<String> serverRecords;
	private Logger logger;
	private boolean secure;
	private ConcurrentHashMap<String, Socket> outgoingConnections;

	/**
	 * Constructs a ServerExchangeTask instance.
	 * 
	 * @param ip
	 *            the host ip address
	 * @param exchangeInterval
	 *            the time interval between two exchange events, in seconds
	 * @param serverRecords
	 *            the server records of the original server
	 * @param logger
	 *            the Logger object of the original server
	 */
	public ServerExchangeTask(String ip, int exchangeInterval, List<String> serverRecords, Logger logger,
			boolean secure, ConcurrentHashMap<String, Socket> outgoingConnections) {
		this.secure = secure;
		this.selfip = ip;
		this.exchangeInterval = exchangeInterval;
		this.serverRecords = serverRecords;
		this.logger = logger;
		this.outgoingConnections = outgoingConnections;
	}

	@Override
	public void run() {
		Socket socket = null;
		while (true) {
			if (serverRecords.size() > 1) {
				Random randomizer = new Random();
				String randomServer = null;
				while (true) {
					randomServer = serverRecords.get(randomizer.nextInt(serverRecords.size()));
					if (!randomServer.equals(selfip)) {
						break;
					}
				}
				String serverHostname = randomServer.split(":")[0];
				int serverPort = Integer.parseInt(randomServer.split(":")[1]);
				DataOutputStream output = null;
				DataInputStream input = null;
				try {
					if (!secure) {
						socket = new Socket(serverHostname, serverPort);
					} else {
						SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
						socket = (SSLSocket) sslsocketfactory.createSocket(serverHostname, serverPort);
					}
					// socket.setSoTimeout(10 * 1000);
					// outgoingConnections.put(randomServer, socket);
					output = new DataOutputStream(socket.getOutputStream());
					input = new DataInputStream(socket.getInputStream());

					JSONObject request = new JSONObject();
					request.put("command", "EXCHANGE");
					JSONArray serverArray = new JSONArray();
					for (String server : serverRecords) {
						if (!server.equals(randomServer)) {
							JSONObject aServer = new JSONObject();
							aServer.put("hostname", server.split(":")[0]);
							aServer.put("port", Integer.parseInt(server.split(":")[1]));
							serverArray.add(aServer);
						}
					}
					request.put("serverList", serverArray);
					logger.fine("SENT TO " + randomServer + ": " + request.toJSONString());
					// write JSON to server
					output.writeUTF(request.toJSONString());
					output.flush();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					serverRecords.remove(serverHostname + ":" + String.valueOf(serverPort));
					System.out.println("Unreachable server: removed from server record.");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					serverRecords.remove(serverHostname + ":" + String.valueOf(serverPort));
					System.out.println("Unreachable server: removed from server record.");
				}
				// receive response from the other server
				JSONParser parser = new JSONParser();
				if (socket != null) {
					while (true) {
						String message = null;
						try {
							message = input.readUTF();
							logger.fine("RECIEVED: " + message);
							JSONObject msgObj = null;
							try {
								msgObj = (JSONObject) parser.parse(message);
								if (msgObj.containsKey("response")) {
									break;
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}
						} catch (IOException e) {
							e.printStackTrace();
							break;
						}
					}
					try {
						socket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			try {
				Thread.sleep(exchangeInterval * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
