package EZShare;

import org.json.simple.JSONObject;

public class Subscriber {
	private String id;
	private ResourceTemplate subscription;
	private boolean relay;
	
	public Subscriber(String id, JSONObject template,boolean relay) throws Exception {
		this.id = id;
		this.subscription = new ResourceTemplate(template);
		this.relay = relay;
		subscription.getName();
		subscription.getDescription();
		subscription.getTags();
		subscription.getUri();
		subscription.getChannel();
		subscription.getOwner();
	}

	public ResourceTemplate getSubscription() {
		return subscription;
	}

	public String getId() {
		return id;
	}

	public boolean isRelay() {
		return relay;
	}

}
