package EZShare;

/**
 * A simple general class that implements a tuple having three elements.
 * 
 * @author yuxin
 *
 * @param <T>
 */
public class Tuple<T> {
	private T first;
	private T second;
	private T third;

	/**
	 * Constructs a Tuple instance
	 * 
	 * @param first
	 *            first element
	 * @param second
	 *            second element
	 * @param third
	 *            third element
	 */
	public Tuple(T first, T second, T third) {
		super();
		this.first = first;
		this.second = second;
		this.third = third;
	}

	/**
	 * Returns the first element
	 * 
	 * @return the first element
	 */
	public T getFirst() {
		return first;
	}

	/**
	 * Returns the second element
	 * 
	 * @return the second element
	 */
	public T getSecond() {
		return second;
	}

	/**
	 * Returns the third element
	 * 
	 * @return the third element
	 */
	public T getThird() {
		return third;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		result = prime * result + ((third == null) ? 0 : third.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Tuple))
			return false;
		Tuple<?> other = (Tuple<?>) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (second == null) {
			if (other.second != null)
				return false;
		} else if (!second.equals(other.second))
			return false;
		if (third == null) {
			if (other.third != null)
				return false;
		} else if (!third.equals(other.third))
			return false;
		return true;
	}

}
